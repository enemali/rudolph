@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				RTC Value of Wafer
				<a href="{{route('home')}}" class="btn btn-default pull-right"> Menu</a>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
							<div class="col-md-6">
								<h5 >Input parameters</h5>

								<div class="form-group row">
									<label class="col-sm-3 control-label">Stepper Type</label>
									<div class="col-md-3">
										<select class="form-control">
											<option>ASML:5500/250C</option>
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-3 control-label">Wafer Size (inches)</label>
									<div class="col-md-3">
										<select class="form-control">
											<option>ASML:5500/250C</option>
										</select>
									</div>
								</div>

								<div class="row form-group">
									<div class="input-group col-md-7">
				                          <span class="input-group-addon">Edge Clearance (mm)</span>
				                          <input type="text" name="customer_name" placeholder="18" class="form-control">
			                         </div>
								</div>

								<div class="row form-group">
									 <div class="input-group col-md-7">
				                          <span class="input-group-addon">X Die size (mm)</span>
				                          <input type="text" name="customer_name" placeholder="22" class="form-control">
			                         </div>
								</div>

								<div class="row form-group">
									 <div class="input-group col-md-7">
				                          <span class="input-group-addon">Y Die size (mm)</span>
				                          <input type="text" name="customer_name" placeholder="33" class="form-control">
			                         </div>
								</div>

							</div>
							<div class="col-md-6">

								<h5>Optimization Options</h5>
								<div class="row">
									<div class="form-group">
										<div class="col-md-1">
											<input type="checkbox" name="">
										</div>
										<label class="col-sm-5 control-label">Rotation of Dies OK ?</label>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-md-1">
											<input type="checkbox" name="">
										</div>
										<label class="col-sm-5 control-label">Half-Shift of Rows OK ?</label>
									</div>
								</div>

								<h5>Exposure Options</h5>
								<div class="row">
									<div class="form-group">
										<div class="col-md-1">
											<input type="radio" name="">
										</div>
										<label class="col-sm-5 control-label">Only full shots ?</label>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-md-1">
											<input type="radio" name="">
										</div>
										<label class="col-sm-5 control-label">Partial Exposure shots OK ?</label>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<div class="col-md-1">
											<input type="radio" name="">
										</div>
										<label class="col-sm-5 control-label">Total Wafer coverage ?</label>
									</div>
								</div>

							</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h5>Determine Wafer Map</h5>
						<div class="col-md-6">

							<div class="row form-group">
									 <div class="input-group col-md-7">
				                          <span class="input-group-addon">X Matrix Shift (mm)</span>
				                          <input type="text" name="customer_name" placeholder="33" class="form-control">
			                         </div>
							</div>

							<div class="row form-group">
									 <div class="input-group col-md-7">
				                          <span class="input-group-addon">Y Matrix Shift (mm)</span>
				                          <input type="text" name="customer_name" placeholder="33" class="form-control">
			                         </div>
							</div>

						</div>
						<div class="col-md-8">
							<button class="btn btn-default">Evaluate</button>
							<button class="btn btn-default">Optimize</button>
							<button class="btn btn-default">Help</button>
							<button class="btn btn-default">Main</button>
							<button class="btn btn-default">Exit</button>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection