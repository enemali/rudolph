@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
			<div class="panel panel-default">
			<div class="panel-heading">
				RTC Cost of Ownership
				<a href="{{route('home')}}" class="btn btn-default pull-right"> Menu</a>
			</div>
			<div class="panel-body">

				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive">
							<thead>
								<th>Stepper Type</th>
								<th>Awg Throughput</th>
								<th>Stepper levels</th>
								<th>Rework / level (%)</th>
								<th>Utilization (%)</th>
							</thead>
							<tbody>
								<tr>
									<th>
										<select class="form-control">
											<option>ASML:5500/250C</option>
										</select>
									</th>

									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
								</tr>

								<tr>
									<th>
										<select class="form-control">
											<option>ASML:5500/250C</option>
										</select>
									</th>

									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
								</tr>

								<tr>
									<th>
										<select class="form-control">
											<option>ASML:5500/250C</option>
										</select>
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
									<th>
										<input type="text" name="" class="form-control">
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<input type="text" name="" class="form-control">
							<label>Scraps overall process steps</label>
						</div>

						<div class="col-md-6"></div>
						<button class="btn btn-default">Mix & Match</button>
						<button class="btn btn-default">Compare</button>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
						<input type="text" name="" class="form-control">
						<select class="form-control">
							<option>Wafer Starts</option>
						</select>
						per
						<select class="form-control">
							<option>Month</option>
						</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-default btn-lg">Main</button>
						</div>
						<div class="col-md-2">
							<button class="btn btn-default btn-lg">OK</button>
						</div>
					</div>
				</div>


			</div>
		</div>
    </div>
</div>
@endsection