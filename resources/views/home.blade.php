@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">Main Menu</div>

                <div class="panel-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="well well-sm">
                            <p class="text-info text-center">Choose on module. Modules can be build upon each other, as shown by the arrows, but can be choosen without unning the predecessors</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="btn-group btn-group-justified" role="group" aria-label="information">
                          <div class="btn-group" role="group">
                          </div>
                          <div class="btn-group" role="group">
                            <a href="{{ route('wafermap') }}" class="btn btn-primary">Wafer Map</a>
                          </div>
                          <div class="btn-group" role="group">
                          </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="text-center">
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        </div>
                    </div>

                      <div class="row">
                        <div class="btn-group btn-group-justified" role="group" aria-label="information">
                          <div class="btn-group" role="group">
                          </div>
                          <div class="btn-group" role="group">
                            <a href="{{ route('throughput') }}" class="btn btn-primary">Throughput</a>
                          </div>
                          <div class="btn-group" role="group">
                          </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="text-center">
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        </div>
                    </div>

                      <div class="row">
                        <div class="btn-group btn-group-justified" role="group" aria-label="information">
                          <div class="btn-group" role="group">
                            <a href="{{ route('capacityplanner') }}" class="btn btn-primary">Capacity Planner</a>
                          </div>
                          <div class="btn-group" role="group">

                          </div>
                          <div class="btn-group" role="group">
                            <a href="{{ route('costofownership') }}" class="btn btn-primary">Cost of Ownership</a>
                          </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      </div>
      <div class="modal-body">

            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title text-center">RTECH Value of Ownership</h3>
              </div>
              <div class="panel-body">

                    <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <img src="images\logo.jpg">
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>RTEC_VOO </p>
                            <p>Version 1.1</p>
                            <p>Released 22/09/2017</p>
                            <p>Expires 22/09/2018</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                            <p class="text-center">The information contained here is property of RTEC.</p>
                            <p class="text-center">Do not distribute without the permision or consent of an authorized staff of RTEC representation</p>
                    </div>
                    <div class="col-md-12 text-center">
                        <div class="col-md-12">
                            <p>Rudolph Technologies Inc</p>
                        </div>

                    </div>
                    <div class="col-md-12 text-center">
                            <p>Attn {{ Auth::user()->name }}<br> 585-642-8100</p>
                    </div>
                    <div class="col-md-12 text-center">

                        <div class="input-group">
                          <span class="input-group-addon">CUSTOMER NAME</span>
                           <input type="text" name="customer_name" value="{{Auth::user()->name}}" placeholder="Enter Customer Name" class="form-control disabled">
                        </div>
                    </div>
                 </div>

              </div>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
@section('pageScript')
<script type="text/javascript">
        $(document).ready(function(){
            // $('#myModal').modal('show');
        });
</script>
@endsection