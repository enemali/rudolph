<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function wafermap()
    {
        return view('wafermap');
    }

    public function throughput()
    {
        return view('throughput');
    }

    public function capacityplanner()
    {
        return view('capacityplanner');
    }

    public function costofownership()
    {
        return view('costofownership');
    }

}
